# -*- coding: utf-8 -*-
"""
Created on Tue Nov 17 15:54:37 2020

@author: Hubert Voogd, based on Pillow (PIL) documentation
"""
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFilter
from psychopy import core
from psychopy import visual
import math

win = visual.Window([1024, 768], fullscr=False, monitor="testMonitor", units='cm')

im1 = Image.open('BergenOostenrijk.jpg').resize((1024, 768)) # This is the image you want to show
im2 = im1.filter(ImageFilter.GaussianBlur(radius = 15)) # This is the blurred version of the same image
blackForeground = Image.new("RGB", im1.size, (0,0,0)) # This is the foreground image... a black surface

mask = Image.new("L", im1.size, 0)
draw = ImageDraw.Draw(mask) #This will make a drawable mask, where we draw our circle on.

imageStim = visual.ImageStim(win,blackForeground) # This is our Psychopy imageStim that draws on the window

radius = 100  # circle radius

for i in range(1440):
    draw.bitmap((0, 0), mask) # clear the circle
    x = math.sin(i * 2 * math.pi / 360) * (( im1.size[0] / 2) - radius) + (im1.size[0] / 2)  # get x value (get it from gazedata!!)
    y = math.cos(i * 2.5 * math.pi / 360) * ((im1.size[1] / 2) - radius) + (im1.size[1] / 2)  # get y value (get it from gazedata!!)
    draw.ellipse((x - radius, y - radius, x + radius, y + radius), fill=255) # draw the circle at (x, y) and with a radius
    #im = Image.composite(im1, blackForeground, mask)  # Create an image where it contains the image (im1) in the circle, and the black foreground outside the circle.
    im = Image.composite(im1, im2, mask)  # Create an image where it contains the image (im1) in the circle, and the blurred version outside the circle.
    imageStim.image = im # set the resulting image in the visual.ImageStim
    imageStim.draw()    # draw to the window
    win.flip()          # flip the window


win.close()
core.quit()