# Torchlight Demo

This is a demo for a 'torchlight', where only a part (a circle) of an image is shown. The part of the image that is within the circle is shown, the part that is outside the circle is shown as a black foreground, or shows a blurred version of the image. 

When the circle center (x, y) is coupled to eyegaze data, the visible part of the image will move along with the eyegaze.